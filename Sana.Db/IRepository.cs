﻿using System.Collections.Generic;

namespace Sana.Db
{
    public interface IRepository<TEntity, in TKey>
    {
        TEntity Get(TKey id);
        bool Create(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(TKey id);
        IEnumerable<TEntity> All();
    }
}
