﻿using System.Collections.Generic;
using System.Linq;
using Sana.Db.Model;

namespace Sana.Db
{
    public class ProductInmemoryRepository : IRepository<Product, int>
    {
        private readonly List<Product> _products;

        public ProductInmemoryRepository()
        {
            _products = new List<Product>();
        }

        public Product Get(int id)
        {
            return _products.FirstOrDefault(p => p.Id == id);
        }

        public bool Create(Product entity)
        {

            if (entity.Id != 0) return false;

            if (_products.Any(p => p.Title == entity.Title)) return false;

            var newId = (!_products.Any() ? 0 : _products.Max(p => p.Id)) + 1;
            entity.Id = newId;
            _products.Add(entity);

            return true;
        }

        public bool Update(Product entity)
        {
            var checkIfExists = Get(entity.Id);

            var isDuplicated = _products.Any(p => p.Title == entity.Title && p.Id != entity.Id);

            if (checkIfExists == default(Product) || isDuplicated) return false;

            var removed = Delete(checkIfExists.Id);

            if (removed)
                _products.Add(entity);

            return removed;
        }

        public bool Delete(int id)
        {
            var checkIfExists = Get(id);

            return checkIfExists != default(Product) && _products.Remove(checkIfExists);
        }

        public IEnumerable<Product> All()
        {
            return _products.OrderBy(p => p.Id);
        }
    }
}
