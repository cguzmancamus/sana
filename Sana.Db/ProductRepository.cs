﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Sana.Db.Model;

namespace Sana.Db
{
    public class ProductRepository : IRepository<Product, int>
    {
        private readonly DbContext<Product> _ctx;

        public ProductRepository(DbContext<Product> dbContext)
        {
            _ctx = dbContext;
        }

        public Product Get(int id)
        {
            const string query = "select Id, Title, Price from Product where Id = @id";
            var idParameter = new SqlParameter("@id", SqlDbType.Int) { Value = id };
            return _ctx.GetEntity(query, new[] { idParameter });
        }

        public bool Create(Product entity)
        {
            if (entity.Id != 0) return false;

            const string query = "INSERT INTO Product(Title,Price) VALUES(@title, @price)";
            var titleParameter = new SqlParameter("@title", SqlDbType.VarChar) { Value = entity.Title };
            var priceParameter = new SqlParameter("@price", SqlDbType.Decimal) { Value = entity.Price };

            try
            {
                return _ctx.Execute(query, new[] { titleParameter, priceParameter }) != 0;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool Update(Product entity)
        {
            var checkIfExists = Get(entity.Id);

            if (checkIfExists == default(Product)) return false;

            const string query = "update Product set Title = @title, Price = @price where Id = @id";
            var titleParameter = new SqlParameter("@title", SqlDbType.VarChar) { Value = entity.Title };
            var priceParameter = new SqlParameter("@price", SqlDbType.Decimal) { Value = entity.Price };
            var idParameter = new SqlParameter("@id", SqlDbType.Int) { Value = entity.Id };

            try
            {
                return _ctx.Execute(query, new[] { titleParameter, priceParameter, idParameter }) != 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            var checkIfExists = Get(id);

            if (checkIfExists == default(Product)) return false;

            const string query = "delete from Product where Id = @id";
            var idParameter = new SqlParameter("@id", SqlDbType.Int) { Value = id };

            try
            {
                return _ctx.Execute(query, new[] {idParameter}) != 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Product> All()
        {
            const string query = "select Id, Title, Price from Product order by Id asc";

            return _ctx.GetDataEntities(query);
        }
    }
}
