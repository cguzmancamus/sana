﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Sana.Db
{
    public class DbContext<TEntity> where TEntity : new()
    {
        private readonly string _strconn;

        public DbContext(string strconn)
        {
            _strconn = strconn;
        }

        public IList<TEntity> GetDataEntities(string query)
        {
            using (var conn = new SqlConnection(_strconn))
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        var entities = new List<TEntity>();

                        while (dr.Read())
                        {
                            entities.Add(CreateObject(dr));
                        }

                        return entities;
                    }
                }
            }
        }

        public TEntity GetEntity(string query, SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(_strconn))
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddRange(parameters);
                    conn.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CreateObject(dr);
                        }
                        return default(TEntity);
                    }
                }
            }
        }

        public int Execute(string query, SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(_strconn))
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddRange(parameters);
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        private TEntity CreateObject(IDataRecord dr)
        {
            var columns = typeof(TEntity).GetProperties().Select(p => p.Name);

            var entity = new TEntity();

            foreach (var c in columns)
            {
                var property = entity.GetType().GetProperty(c);
                property?.SetValue(entity, Convert.ChangeType(dr[c], property.PropertyType) );
            }

            return entity;
        }
    }
}
