﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sana.Controllers
{
    public class PersistenceController : BaseController
    {
        // GET: Persistence
        public ActionResult Change()
        {
            HttpContext.Application["IsInMemory"] = !(bool)HttpContext.Application["IsInMemory"];

            return RedirectToAction("Index", "Product");
        }
    }
}