﻿using System.Web.Mvc;
using Sana.Db.Model;
using Sana.Models;

namespace Sana.Controllers
{
    public class ProductController : BaseController
    {
        public ActionResult Index(bool? error)
        {
            if (error.HasValue)
                ViewBag.HasError = error.Value;

            var productListviewModel = new ProductListViewModel()
            {
                Products = Repository.All()
            };
            return View(productListviewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var product = Repository.Get(id);

            if (product == default(Product)) return HttpNotFound();

            return View(new ProductViewModel(product));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel product)
        {
            if (!ModelState.IsValid) return View(product);

            var result = Repository.Update(product.Product);

            return RedirectToAction(nameof(Index), new { error = !result });
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ProductViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel product)
        {
            if (!ModelState.IsValid) return View(product);

            var result = Repository.Create(product.Product);

            return RedirectToAction(nameof(Index), new { error = !result });
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var product = Repository.Get(id);

            if (product == default(Product)) return HttpNotFound();

            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var result = Repository.Delete(id);
            return RedirectToAction(nameof(Index), new { error = !result });
        }
    }
}