﻿using System.Web.Mvc;
using System.Web.Routing;
using Sana.Db;
using Sana.Db.Model;

namespace Sana.Controllers
{
    public class BaseController : Controller
    {
        protected IRepository<Product, int> Repository { get; private set; }

        public bool IsInMemory => (bool) HttpContext.Application["IsInMemory"];

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if ((bool)HttpContext.Application["IsInMemory"])
                Repository = (IRepository<Product, int>)HttpContext.Application["Repository"];
            else
            {
                var ctx = new DbContext<Product>(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                Repository = new ProductRepository(ctx);
            }
        }
    }
}