﻿using System.ComponentModel.DataAnnotations;

namespace Sana.CustomValidation
{
    public class PositiveNumberAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return (decimal) value >= 0 ? ValidationResult.Success : new ValidationResult("Negative value");
        }
    }
}