﻿using System.ComponentModel.DataAnnotations;
using Sana.CustomValidation;
using Sana.Db.Model;

namespace Sana.Models
{
    public class ProductViewModel
    {
        [Required(ErrorMessage = "{0} cannot be empty")]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} cannot be empty")]
        [StringLength(128)]
        public string Title { get; set; }

        [Required(ErrorMessage = "{0} cannot be empty")]
        [DataType(DataType.Currency)]
        [PositiveNumber]
        public decimal Price { get; set; }

        public ProductViewModel(Product product)
        {
            Id = product.Id;
            Title = product.Title;
            Price = product.Price;
        }

        public ProductViewModel() { }

        public Product Product => new Product() { Id = Id, Title = Title, Price = Price };
    }
}