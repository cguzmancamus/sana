﻿using System.Collections.Generic;
using Sana.Db.Model;

namespace Sana.Models
{
    public class ProductListViewModel
    {
        public IEnumerable<Product> Products { get; set; }
    }
}